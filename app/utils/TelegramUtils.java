package utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import models.*;
import play.libs.WS;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * Created by alejandro on 5/19/16.
 */
public class TelegramUtils {
    public static final String BASE_API_URL = "https://api.telegram.org/bot214517201:AAGiuPLFc1zMFq5SN556PsfxvEf40yXcX8o";

    public static void sendMessage(Message message){
        try {
            WS.url(BASE_API_URL + "/sendMessage")
                    .setParameter("text", message.text)
                    .setParameter("chat_id", message.chat.chatId)
                    .get().getJson();
        } catch (Exception e){
            System.out.println("Error al enviar mensaje");
        }
    }

    public static void sendMessage(String text, String chatId){
        try {
            WS.url(BASE_API_URL + "/sendMessage")
                    .setParameter("text", text)
                    .setParameter("chat_id", chatId)
                    .get().getJson();
        } catch (Exception e){
            System.out.println("Error al enviar mensaje");
        }
    }

    public static void sendInlineKeyboardMessage(String text, String chatId, List<InlineKeyboard> inlineKeyboards){
        try {
            WS.url(BASE_API_URL + "/sendMessage")
                    .setParameter("text", text)
                    .setParameter("chat_id", chatId)
                    .setParameter("reply_markup", new Gson().toJson(inlineKeyboards))
                    .get().getJson();
        } catch (Exception e){
            System.out.println("Error al enviar mensaje");
        }
    }

    public static void sendPhoto(String url, String chatId){
        try {
            WS.url(BASE_API_URL + "/sendMessage")
                    .setParameter("photo", url)
                    .setParameter("chat_id", chatId)
                    .setHeader("Content-Type", "multipart/form-data")
                    .get().getJson();
        } catch (Exception e){
            System.out.println("Error al enviar foto");
        }
    }

    public static void getFlaco(String body){
        try {
            JsonElement json = new JsonParser().parse(body).getAsJsonObject().get("message");
            Message message = messageCreator(json);
            reminderAnalyzer(message);
            createStats(message);
            MessageAnalysisUtils.messageAnalyzer(message);
        } catch (Exception e){
            System.out.println("Error en getFlaco");
            System.out.println(e);
            System.out.println(body);
        }
    }

    public static void createStats(Message message){
        if (message.text.startsWith("/pickclass")){
            System.out.println("llego a pickclass");
            List<InlineKeyboard> inlineKeyboards = Lists.newArrayList(
                    new InlineKeyboard("Warrior", "https://flaqueador.herokuapp.com/flaqueador/getFlaco", Stats.WARRIOR),
                    new InlineKeyboard("Archer", "https://flaqueador.herokuapp.com/flaqueador/getFlaco", Stats.ARCHER),
                    new InlineKeyboard("Mage", "https://flaqueador.herokuapp.com/flaqueador/getFlaco", Stats.MAGE)
            );
            sendInlineKeyboardMessage("Elegí con que clase empezar (Eventualmente podes maxear en todas las clases)", message.chatId, inlineKeyboards);
        }
    }

    public static Message messageCreator(JsonElement json){
        try {
            Message message = new Message();
            System.out.println(json);
            message.text = json.getAsJsonObject().get("text").getAsString();
            String chatId = json.getAsJsonObject().get("chat").getAsJsonObject().get("id").getAsString();
            message.chatId = chatId;
            Chat chat = Chat.findByChatId(chatId);
            if (chat==null){
                chat = new Chat();
                chat.chatId = chatId;
                chat.save();
            } else {
                message.chat = chat;
            }

            //Genero un user en base al mensaje que llega, y se lo meto a message
            String userId = json.getAsJsonObject().get("from").getAsJsonObject().get("id").getAsString();
            User user = User.findByUserId(userId);
            if (user==null){
                user = new User();
                user.id = userId;
                user.chats.add(chat);
                user.firstName = json.getAsJsonObject().get("from").getAsJsonObject().get("first_name").getAsString();
                user.save();
            }
            if (!User.alreadyHasChat(chatId, userId)) {
                user.chats.add(chat);
                user.save();
                System.out.println("Agrego el chat re piola :D");
            }
            message.user = user;
            message.userId = user.id;
            message.save();
            return message;
        } catch (Exception e){
            System.out.println("Error al crear mensaje");
            System.out.println(e);
            return null;
        }
    }

    public static void rochify(Message message){
        if (message.text.indexOf("!") == 0) {
            if (message.text.startsWith("!roch")) {

            }
        }
    }

    public static void reminderAnalyzer(Message message){
        try {
            if (message.text.toLowerCase().contains("pasame la repo")){
                TelegramUtils.sendMessage("https://bitbucket.org/alejrad/flaqueador/commits/all", message.chat.chatId);
            }
            if (message.text.indexOf("!") == 0) {
                if (message.text.startsWith("!remind") || message.text.startsWith("!record")) {
                    ReminderUtils.createReminder(message);
                    return;
                }
                if (message.text.startsWith("!myremind") ||  message.text.startsWith("!misrecord")){
                    ReminderUtils.listReminders(message);
                    return;
                }
                if (message.text.startsWith("!metrics")){
                    List<Message> messageList  = Message.filter("chatId", message.chat.chatId).asList();
                    TelegramUtils.sendMessage("Hubieron "+ messageList.size() + " mensajes en este chat desde que active el meme de las metricas", message.chat.chatId);
                    List<User> users = User.findByChatId(message.chatId);
                }
                if (message.text.startsWith("!memetrics")){
                    List<User> users = message.chat.users;
                    HashMap<String, Integer> map = Maps.newHashMap();
                    for (User user : users) {
                        map.put(user.firstName, user.getMessageCount());
                    }
                    map = sortByValue(map);
                    StringBuilder sb = new StringBuilder();
                    long count = Message.count();
                    for (Map.Entry entry : map.entrySet()){
                        double percentage = round(Double.parseDouble(entry.getValue().toString()) * 100 / count, 2);
                        sb.append(entry.getKey() + " mandó " + entry.getValue() + " mensajes. (El " + percentage + "% de los mensajes)\n");
                    }
                    TelegramUtils.sendMessage(sb.toString(), message.chatId);
                    return;
                }
                if (message.text.startsWith("!topwords")){
                    int n = Integer.parseInt(RegexUtils.findInString("\\d+", message.text));
                    List<Message> messages = Message.findByChat(message.chatId);
                    List<String> mtxt = Lists.newArrayList();
                    for (Message message1 : messages){
                        String[] words = message1.text.split("\\s");
                        for (String word: words){
                            if (word.length()>3) {
                                mtxt.add(word.toLowerCase());
                            }
                        }
                    }
                    String[] topFrequentArray = mtxt.toArray(new String[0]);
                    topFrequentArray = AlgorithmUtils.topKFrequentWords(topFrequentArray,n);
                    StringBuilder sb = new StringBuilder();
                    for (String word: topFrequentArray){
                        sb.append(word+"\n");
                    }
                    TelegramUtils.sendMessage(sb.toString(),message.chatId);
                    return;
                }
                if (message.text.startsWith("!mytopwords")){
                    int n = Integer.parseInt(RegexUtils.findInString("\\d+", message.text));
                    if (n>50){
                        n=50;
                    }
                    List<Message> messages = Message.findByUser(message.user);
                    List<String> mtxt = Lists.newArrayList();
                    for (Message message1 : messages){
                        String[] words = message1.text.split("\\s");
                        for (String word: words){
                            if (word.length()>3) {
                                mtxt.add(word.toLowerCase());
                            }
                        }
                    }
                    String[] topFrequentArray = mtxt.toArray(new String[0]);
                    topFrequentArray = AlgorithmUtils.topKFrequentWords(topFrequentArray,n);
                    StringBuilder sb = new StringBuilder();
                    for (String word: topFrequentArray){
                        sb.append(word+"\n");
                    }
                    TelegramUtils.sendMessage(sb.toString(),message.chatId);
                    return;
                }
            }
            if (message.user.isAwaitingForReminderOptions){
                ReminderUtils.getReminderForList(message);
                return;
            }

            if (message.user.isAwaitingToEdit){
                ReminderUtils.editReminder(message);
                return;
            }

            if (message.user.isAwaitingToEditDate){
                ReminderUtils.editReminderDate(message);
                return;
            }

        } catch (Exception e) {
            System.out.println("Error al parsear remind me");
            System.out.println(e);
        }
    }


    private static <K, V> HashMap<K, V> sortByValue(HashMap<K, V> map) {
        List<Map.Entry<K, V>> list = Lists.newLinkedList(map.entrySet());
        Collections.sort(list, new Comparator<Object>() {
            @SuppressWarnings("unchecked")
            public int compare(Object o2, Object o1) {
                return ((Comparable<V>) ((Map.Entry<K, V>) (o1)).getValue()).compareTo(((Map.Entry<K, V>) (o2)).getValue());
            }
        });

        HashMap<K, V> result = Maps.newLinkedHashMap();
        for (Iterator<Map.Entry<K, V>> it = list.iterator(); it.hasNext();) {
            Map.Entry<K, V> entry = (Map.Entry<K, V>) it.next();
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
