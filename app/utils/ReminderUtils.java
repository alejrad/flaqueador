package utils;

import models.Chat;
import models.Message;
import models.Reminder;
import models.User;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandro on 5/20/16.
 */
public class ReminderUtils {

    public static void listReminders(Message message){
        try {
            List<Reminder> myReminders = Reminder.findByUserId(message.user.id);
            String myRemindersText = "Flaco tus reminders son: \n";
            int i = 0;
            for (Reminder reminder : myReminders) {
                i++;
                myRemindersText = myRemindersText + i + " - " + reminder.message.getTextToRemind() + "\n";
            }
            if (myReminders != null && myReminders.size() < 1) {
                TelegramUtils.sendMessage("Flaco no tenes reminders, media pila no gastes recursos del servidor al pedo", message.chat.chatId);
                return;
            }
            TelegramUtils.sendMessage(myRemindersText, message.chat.chatId);

            User user = User.findByUserId(message.user.id);
            user.clearWaitingStatus(user);
            user.isAwaitingForReminderOptions = true;
            user.save();
        }catch(Exception e){
            System.out.println("Error al listar " + e);
        }
    }

    public static void createReminder(Message message){
        Date remindDate = RegexUtils.daysParser(message.text);
        Reminder reminder = Reminder.create(remindDate, message);
        reminder.save();
        TelegramUtils.sendMessage("Flaco yo te recuerdo: " + message.getTextToRemind() + reminder.timeUntillReminder(reminder), message.chat.chatId);
    }

    public static void getReminderForList(Message message){
        if (message.text.matches("^(\\d+).*")) {
            String number = RegexUtils.findInString("^(\\d+)(.*)", message.text);
            List<Reminder> reminders = Reminder.findByUserId(message.user.id);
            Reminder reminder = reminders.get(Integer.parseInt(number) - 1);
            TelegramUtils.sendMessage("Selecciona que queres hacer con este reminder\n 1 - Editar Fecha \n 2- Borrar", reminder.message.chat.chatId);
            User user = message.user;
            user.isAwaitingForReminderOptions = false;
            user.isAwaitingToEdit = true;
            user.lastReminderSelected = reminder;
            user.save();
            return;
        }
    }

    public static void editReminder(Message message){
        if (message.text.matches("^(\\d+).*")) {
            int number = Integer.parseInt(RegexUtils.findInString("^(\\d+)(.*)", message.text));
            User user = User.findByUserId(message.user.id);
            Chat chat = Chat.findByChatId(message.chat.chatId);
            if (number==2){
                Reminder reminder = user.lastReminderSelected;
                reminder.delete();
                user.lastReminderSelected = null;
                TelegramUtils.sendMessage("OK, borre el recordatioro \"" + reminder.message.getTextToRemind() + "\".", chat.chatId);
            }
            if (number==1){
                user.isAwaitingToEditDate = true;
                TelegramUtils.sendMessage("Escribí en cuantos dias/horas/minutos queres que sea el nuevo recordatorio", chat.chatId);
            }
            user.isAwaitingToEdit=false;
            user.save();
        }
    }

    public static void editReminderDate(Message message) {
        if (message.text.matches(".*(en|in)?\\s?\\d+\\s?(dias?|days?|horas?|hours?|minutes?|minutos?).*")) {
            try {
                User user = User.findByUserId(message.user.id);
                Reminder reminder = user.lastReminderSelected;
                Chat chat = Chat.findByChatId(message.chat.chatId);
                if (reminder!=null){
                    Date date = RegexUtils.daysParser(message.text);
                    if (date != null) {
                        reminder.expiryDate = date;
                        reminder.save();
                        TelegramUtils.sendMessage("Ok, te lo voy a recordar " + reminder.timeUntillReminder(reminder), chat.chatId);
                    }
                    user.lastReminderSelected = null;
                    user.isAwaitingToEditDate = false;
                    user.save();
                }
            } catch (Exception e){
                System.out.println("Se rompio en el editar date");
                System.out.println(e);
            }
        }
    }
}
