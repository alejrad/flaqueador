package utils;

import models.Message;
import models.User;

/**
 * Created by alejandro on 5/25/16.
 */
public class MessageAnalysisUtils {

    public static void messageAnalyzer(Message message) {
        if (message.text.matches("(?i).*(no\\s?puedo).*")){
            User user = User.findByUserId(message.user.id);
            if ("159545586".equals(user.id)) { // Matt
                TelegramUtils.sendMessage("No puedo, estoy viendo un partido porque soy un autista que ve gente patear un esférico", message.chat.chatId);
            }
            if ("117116661".equals(user.id)) { //Nachoco
                TelegramUtils.sendMessage("Flaco yo laburo, estudio y tengo novia", message.chat.chatId);
            }

            if ("224870586".equals(user.id)){ //yo
                TelegramUtils.sendMessage("Flaco sos malisimo no te necesitamos", message.chat.chatId);
            }

            if ("125779633".equals(user.id)){
                TelegramUtils.sendMessage("Flaco tengo que acompañar a mi hermana", message.chat.chatId);
            }

            if ("162914725".equals(user.id)){
                TelegramUtils.sendMessage("Mentira, si yo siempre me prendo a todo", message.chat.chatId);
            }

            if ("162137318".equals(user.id)){
                TelegramUtils.sendMessage("Me da paja existir", message.chat.chatId);
            }
            return;
        }

        if (message.text.matches("(?i).*(vale|novia|pollera).*")) {
            TelegramUtils.sendMessage("Flaco tengo novia", message.chat.chatId);
            return;
        }
        if (message.text.matches("(?i).*(laburo|trabajo).*")) {
            TelegramUtils.sendMessage("Flaco yo laburo", message.chat.chatId);
            return;
        }
        if (message.text.matches("(?i).*(estudio|facultad|estudiar).*")) {
            TelegramUtils.sendMessage("Flaco yo estudio", message.chat.chatId);
            return;
        }
    }
}
