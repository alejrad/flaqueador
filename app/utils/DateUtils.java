package utils;

import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.collect.Maps;

public class DateUtils {

    public static Date getCurrentDate() {
        return new Date();
    }

    public static Date aMonthFromNow() {
        return new DateTime().plusDays(30).toDate();
    }

    public static Date lastMonth() {
        Date now = new Date();
        return org.apache.commons.lang.time.DateUtils.addMonths(now, -1);
    }

    public static Date aWeekFromNow() {
        Date now = new Date();
        return org.apache.commons.lang.time.DateUtils.addWeeks(now, 1);
    }


    public static long getMillisOfDay(Date date) {
        return new DateTime(date).withMillisOfDay(0).getMillis();
    }

    public static DateTime[] getDatePeriod(String datePeriod, String dateFrom, String dateTo, String datePattern) {
        if (StringUtils.isEmpty(datePeriod)) {
            datePeriod = "last-30-days";
        }

        DateTime datetimeFrom = null;
        DateTime datetimeTo = null;
        if (StringUtils.equals("TODAY", datePeriod)) {
            datetimeFrom = DateTime.now();
            datetimeTo = datetimeFrom;
        } else if (StringUtils.equals("yesterday", datePeriod)) {
            datetimeFrom = DateTime.now().minusDays(1);
            datetimeTo = datetimeFrom;
        } else if (StringUtils.equals("LAST_7_DAYS", datePeriod)) {
            datetimeFrom = DateTime.now().minusDays(7);
            datetimeTo = DateTime.now();
        }
        else if (StringUtils.equals("THIS_WEEK_MON_TODAY", datePeriod)) {
            datetimeFrom = DateTime.now().minusDays(DateTime.now().getDayOfWeek()).plusDays(1);
            System.out.println("from posta: " + datetimeFrom.toString());
            datetimeTo = DateTime.now();
        }
        else if (StringUtils.equals("THIS_MONTH", datePeriod)) {
            datetimeFrom = DateTime.now().minusDays(DateTime.now().getDayOfMonth()+1);
            datetimeTo = DateTime.now();
        }
        else if (StringUtils.equals("last-week", datePeriod)) {
            datetimeFrom = DateTime.now().withDayOfWeek(1).minusWeeks(1);
            datetimeTo = DateTime.now().withDayOfWeek(1).minusDays(1);
        } else if (StringUtils.equals("LAST_MONTH", datePeriod)) {
            datetimeFrom = DateTime.now().withDayOfMonth(1).minusMonths(1);
            datetimeTo = DateTime.now().withDayOfMonth(1).minusDays(1);
        } else if (StringUtils.equals("last-quarter", datePeriod)) {
            int monthsToStartThisQuarter = (DateTime.now().getMonthOfYear() -1) % 3;
            datetimeFrom = DateTime.now().withDayOfMonth(1).minusMonths(monthsToStartThisQuarter + 3);
            datetimeTo = DateTime.now().withDayOfMonth(1).minusMonths(monthsToStartThisQuarter).minusDays(1);
        } else if (StringUtils.equals("last-year", datePeriod)) {
            datetimeFrom = DateTime.now().withDayOfMonth(1).withMonthOfYear(1).minusYears(1);
            datetimeTo = DateTime.now().withDayOfMonth(1).withMonthOfYear(1).minusDays(1);
        } else if (StringUtils.equals("custom", datePeriod) && StringUtils.isNotEmpty(dateFrom) && StringUtils.isNotEmpty(dateTo)) {
            DateTimeFormatter datetimeFormatter = DateTimeFormat.forPattern(datePattern);
            datetimeFrom = datetimeFormatter.parseDateTime(dateFrom);
            datetimeTo = datetimeFormatter.parseDateTime(dateTo);
        } else if (StringUtils.equals("ALL_TIME", datePeriod)) {
            datetimeFrom = DateTime.now().minusYears(10);
            datetimeTo = DateTime.now();
        } else { // "last-30-days"
            datetimeFrom = DateTime.now().minusDays(30);
            datetimeTo = DateTime.now();
        }
        return new DateTime[]{ datetimeFrom, datetimeTo };
    }


    public static Map<Long, Long> newInitializedMapPerDay(DateTime datetimeFrom, DateTime datetimeTo, long defaultValue) {
        TreeMap<Long, Long> mapPerDay = Maps.newTreeMap(new Comparator<Long>() {
            @Override
            public int compare(Long o1, Long o2) {
                return o1.compareTo(o2);
            }
        });
        DateTime dayIdx = datetimeFrom.withMillisOfDay(0);
        int days = Days.daysBetween(datetimeFrom.toDateMidnight(), datetimeTo.toDateMidnight()).getDays() +1;
        for (int i = 0; i < days; i++) {
            mapPerDay.put(dayIdx.getMillis(), defaultValue);
            dayIdx = dayIdx.plusDays(1);
        }
        return mapPerDay;
    }

    public static boolean isOlderThanDays(Date date, int i) {
        Date currentDate = getCurrentDate();
        Date floorDate = org.apache.commons.lang.time.DateUtils.addDays(currentDate, -i);
        return (date.before(floorDate));
    }

    public static Date getDateNMonthAhead(int n) {
        return org.apache.commons.lang.time.DateUtils.addMonths(getCurrentDate(), n);
    }

    public static Date getDateNDayAhead(int n) {
        return getDateNDaysAhead(getCurrentDate(), n);
    }

    public static Date getDateNDaysAgo(int n) {
        return getDateNDaysAhead(getCurrentDate(), n);
    }

    public static Date getDateNDaysAhead(Date date, int n) {
        return org.apache.commons.lang.time.DateUtils.addDays(date, n);
    }

    public static Date getDateNDayAgo(Date date, int n) {
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        return new Date(date.getTime() - (n * DAY_IN_MS));
    }

    public static int getDaysBetween(Date initialDate, Date endDate) {
        if (initialDate == null) {
            initialDate = new Date();
        }
        DateTime start = new DateTime(initialDate);
        DateTime end = new DateTime(endDate);
        int daysBetween = Days.daysBetween(start, end).getDays();
        return daysBetween;

    }

    public static int getMinutesBetween(Date initialDate, Date endDate) {
        if (initialDate == null) {
            initialDate = new Date();
        }
        DateTime start = new DateTime(initialDate);
        DateTime end = new DateTime(endDate);
        int minutesBetween = Minutes.minutesBetween(start, end).getMinutes();
        return minutesBetween;

    }

    public static Date getDateNMinutesAhead(int n) {
        return org.apache.commons.lang.time.DateUtils.addMinutes(getCurrentDate(), n);

    }
}