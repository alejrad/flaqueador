package utils;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alejandro on 5/19/16.
 */
public class RegexUtils {

    public static String findInString(String regex, String text){
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(text);
        while (m.find()) {
            String s = m.group(0);
            return s;
        }
        return "";
    }

    public static Date daysParser(String message){
        String days = "";
        System.out.println(message);
        if (message.matches(".*(days?|dias?).*")){
            days = RegexUtils.findInString("((in\\s?\\d+\\s?days?)|(en\\s?\\d+\\s?dias?))", message);
            days = RegexUtils.findInString("\\d+", days);
            return DateUtils.getDateNDayAhead(Integer.parseInt(days));
        }

        if (message.matches(".*(horas?|hours?).*")){
            days = RegexUtils.findInString("((in\\s?\\d+\\s?hours?)|(en\\s?\\d+\\s?horas?))", message);
            days = RegexUtils.findInString("\\d+", days);
            int hours = Integer.parseInt(days) * 60;
            return DateUtils.getDateNMinutesAhead(hours);
        }

        if (message.matches(".*(minutes?|minutos?).*")){
            days = RegexUtils.findInString("((in\\s?\\d+\\s?minutes?)|(en\\s?\\d+\\s?minutos?))", message);
            days = RegexUtils.findInString("\\d+", days);
            int minutes = Integer.parseInt(days);
            return DateUtils.getDateNMinutesAhead(minutes);
        }

        if (message.matches(".*(tomorrow|mañana).*")){
            return DateUtils.getDateNDayAhead(1);
        }

        if (message.matches(".*(next\\s?week|semana\\s?que\\s?viene|proxima\\s?semana).*")){
            return DateUtils.getDateNDayAhead(7);
        }

        return null;
    }
}
