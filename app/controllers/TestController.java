package controllers;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.JsonElement;
import com.sun.org.apache.regexp.internal.RE;
import models.Chat;
import models.Message;
import models.User;
import play.libs.WS;
import play.mvc.Controller;
import utils.RegexUtils;
import utils.TelegramUtils;

import java.util.*;

/**
 * Created by alejandro on 5/27/16.
 */
public class TestController extends Controller {

    public static void testPhoto(){
            renderJSON(WS.url(TelegramUtils.BASE_API_URL + "/sendMessage")
                    .setParameter("photo", "http://i.imgur.com/2iLiB6r.jpg")
                    .setParameter("chat_id", "12577963")
                    .setHeader("Content-Type", "multipart/form-data")
                    .get().getJson());
    }

    public static void sendBroadcast(String message){
        List<Chat> chats = Chat.findAll();
        for (Chat chat : chats){
            TelegramUtils.sendMessage(message, chat.chatId);
        }
    }

    public static void sendMessage(String message){
        Chat chat = Chat.findByChatId("-145845573");
        TelegramUtils.sendMessage(message, chat.chatId);
    }

    public static void addMsg(){
        response.chunked = true;
        response.contentType = "text/html";
        List<Message> messages = Message.findAll();
        for (Message message : messages){
            response.writeChunk("Listo uno <br>");
            message.chatId = message.chat.chatId;
            message.save();
        }
        response.writeChunk("Lesto todo");
    }

    public static void setUsersForChat(){
        List<User> users = User.findAll();
        for (User user : users){
            List<Chat> chats = user.chats;
            for (Chat chat : chats){
                if (chat.users.size()==0){
                    chat.users = Lists.newArrayList();
                }
                chat.users.add(user);
                chat.save();
            }
        }
    }

    public static void setUserid(){
        response.contentType = "text/html";
        response.chunked = true;
        List<Message> messages = Message.findAll();
        for (Message message : messages){
            if (message.user == null) continue;
            if (message.userId!=null) continue;
            message.userId = message.user.id;
            message.save();
            response.writeChunk("Guardo <br>");
        }
        response.writeChunk("Termino geege");
    }

    public static void getChat(String chatId){
        try {
            JsonElement json = WS.url(TelegramUtils.BASE_API_URL + "/getChat")
                    .setParameter("chat_id", chatId)
                    .get().getJson();
            renderJSON(json);
        } catch (Exception e){
            System.out.println("Error al enviar foto");
        }
    }

    public static void testCommon(){
//        ArrayList<String> list = Lists.newArrayList("meme","capo","de","la","meme","capo","meme","faso","pepe");
//        List<String> mostOcurrent = Lists.newArrayList();
//        int biggestSize;
//        String[] array = list.toArray(new String[0]);
////        renderJSON(topKFrequentWords(array,2));
//        renderJSON(Collections.frequency(list,"meme"));
        renderJSON(RegexUtils.findInString("\\d+","!topwords 5"));

    }



//
//
//    public static void testSort(){
//        HashMap<String,Integer> map = Maps.newHashMap();
//        map.put("Manco", 10);
//        map.put("Puto", 20);
//        map.put("Maricon", 5);
//        map.put("Facho", 3);
//        map.put("Jorge", 3020);
//        map = sortByValue(map);
//        renderJSON(map);
//
//    }

}
