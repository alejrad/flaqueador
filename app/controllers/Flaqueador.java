package controllers;

import play.libs.WS;
import play.mvc.Controller;
import utils.TelegramUtils;

import java.util.Date;

/**
 * Created by alejandro on 5/19/16.
 */
public class Flaqueador extends Controller{
    public static final String BASE_API_URL = "https://api.telegram.org/bot214517201:AAGiuPLFc1zMFq5SN556PsfxvEf40yXcX8o";
    public static final String BASE_API_LOCALHOST = "https://api.telegram.org/bot205423901:AAFIIhqy4zb3E_0_HHJCokgEgGyqpFRapvM";

    public static void setWebHook(){
        String hook = "https://flaqueador.herokuapp.com/flaqueador/getFlaco";
        renderJSON(WS.url(BASE_API_URL+"/setWebhook")
                .setParameter("url", hook)
                .post().getJson());

    }

    public static void setLocalhostHook(){
        String localhook = "https://b1c1a9d8.ngrok.io/flaqueador/getFlaco";
        renderJSON(WS.url(BASE_API_LOCALHOST+"/setWebhook")
                .setParameter("url", localhook)
                .post().getJson());
    }

    public static void getFlaco(){
        String body = params.get("body");
        TelegramUtils.getFlaco(body);
    }

    public static void getServerTime(){
        renderJSON(new Date());
    }
}
