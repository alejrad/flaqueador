package models;

/**
 * Created by alejandro on 7/7/16.
 */
public class InlineKeyboard {
    String text;
    String url;
    String callback_data;
    String switch_inline_query;

    public InlineKeyboard(String text, String url, String callback_data) {
        this.text = text;
        this.url = url;
        this.callback_data = callback_data;
    }
}
