package models;

import com.google.common.collect.Lists;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Reference;
import play.modules.morphia.Model;

import java.util.List;

/**
 * Created by alejandro on 5/20/16.
 */
@Entity
public class Chat extends Model {
    public String chatId;
    @Reference
    public List<User> users = Lists.newArrayList();

    public static Chat findByChatId(String chatId){
        return Chat.find("chatId", chatId).first();
    }

    public static boolean isUserInChat(String userId, String chatId){
        User user = User.findByUserId(userId);
        return false;
    }
}
