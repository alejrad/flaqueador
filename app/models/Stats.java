package models;

/**
 * Created by alejandro on 7/7/16.
 */
public class Stats {
    public static final String WARRIOR="WARRIOR";
    public static final String ARCHER ="ARCHER";
    public static final String MAGE="MAGE";

    public int hitPoints;
    public int defence;
    public int attack;
    public int strength;
    public int magic;
    public int ranged;
    public int speed;
    public int evasion;
    public int accuracy;
    public int luck;
    public int level;
    public int experience;
    public int prayer;

    public Stats(int speed, int evasion, int accuracy, int luck){
        this.hitPoints = 10;
        this.defence = 1;
        this.attack = 1;
        this.strength = 1;
        this.magic = 1;
        this.ranged = 1;
        this.speed = 1 + speed;
        this.evasion = 1 + evasion;
        this.accuracy = 1 + accuracy;
        this.luck = 1 + luck;
        this.level = 1;
        this.experience = 1;
        this.prayer = 1;
    }

    public int getLevel(){
        Double baseLevel = (0.25*(defence+hitPoints+Math.floor(prayer/2)));
        Double melee = 0.325*(attack+strength);
        Double rangedlvl = 0.325*(Math.floor(ranged/2)+ranged);
        Double mage = 0.325*(Math.floor(magic/2)+magic);
        Double cbLvl = (baseLevel + Math.max(Math.max(melee, rangedlvl), mage));
        return cbLvl.intValue();
    }
}
