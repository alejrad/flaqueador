package models;

import org.mongodb.morphia.annotations.Entity;
import play.modules.morphia.Model;
import utils.DateUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandro on 5/19/16.
 */
@Entity
public class Reminder extends Model{
    public Date expiryDate;
    public Message message;
    String userId;

    public static Reminder create(Date expiryDate, Message message){
        Reminder reminder = new Reminder();
        message.save();
        reminder.expiryDate = expiryDate;
        reminder.message = message;
        reminder.userId = message.user.id;
        reminder.save();
        return reminder;
    }

    public static String timeUntillReminder(Reminder reminder){
        int minutesBetween = DateUtils.getMinutesBetween(new Date(), reminder.expiryDate);

        //Mayor a un dia
        if (minutesBetween >= 60*24){
            int dias = minutesBetween/1440;
            if (dias==1 || dias==0){
                return " en 1 dia.";
            }
            return " en "+ dias + " dias.";

        }

        //Mayor a una hora
        else if (minutesBetween>=60){
            if (minutesBetween/60==1 || minutesBetween/60==0){
                return " en 1 hora.";
            }
            return " en " + minutesBetween/60 + " horas.";

        }

        else if (minutesBetween<=60){
            if (minutesBetween==1 || minutesBetween==0){
                return " en 1 minuto.";
            }
            return " en " + minutesBetween + " minutos.";

        }
        return "";
    }

    public static List<Reminder> findByUserId(String userId){
        List<Reminder> reminders = Reminder.filter("userId", userId).filter("expiryDate >=", new Date()).asList();
        return reminders;
    }
}
