package models;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Reference;
import play.modules.morphia.Model;

import java.util.List;

/**
 * Created by alejandro on 5/20/16.
 */
@Entity
public class Message extends Model {
    public String text;
    public Chat chat;
    public User user;
    public String chatId;
    public String userId;
    public InlineKeyboard inlineKeyboardMarkup;

    public String getTextToRemind(){
        return this.text.replaceAll("!remindme ", "").replaceAll("( in \\d+.*)", "").replaceAll("( en \\d+.*)", "").replaceAll("tomorrow", "");
    }

    public static List<Message> findByUser(User user){
        return Message.filter("userId", user.id).asList();
    }

    public static List<Message> findByChat(String chatId){
        return Message.filter("chatId", chatId).asList();
    }
}
