package models;

import com.google.common.collect.Lists;
import org.mongodb.morphia.annotations.Entity;
import play.modules.morphia.Model;

import java.util.List;

/**
 * Created by alejandro on 5/20/16.
 */
@Entity
public class User extends Model {
    public String id;
    public String firstName;
    public String lastName;
    public Boolean isAwaitingForReminderOptions=false;
    public Boolean isAwaitingToEdit=false;
    public Boolean isAwaitingToEditDate=false;
    public Reminder lastReminderSelected;
    public List<Chat> chats = Lists.newArrayList();
    public Stats stats;


    public static boolean alreadyHasChat(String chatId, String userId){
        User user = User.findByUserId(userId);
        Chat chat = Chat.findByChatId(chatId);
        if (user.chats.contains(chat)){
            return true;
        }
        return false;
    }

    public int getMessageCount(){
        return Message.findByUser(this).size();
    }

    public static User findByUserId(String userId){
        return User.find("id", userId).first();
    }

    public static List<User> findByChatId(String chatId){
        return User.find("chatId", chatId).asList();
    }

    public static void clearWaitingStatus(User user){
        user.isAwaitingToEdit = false;
        user.isAwaitingToEditDate =false;
        user.isAwaitingForReminderOptions = false;
        user.save();
    }
}
