package jobs;

import models.Reminder;
import play.jobs.Every;
import play.jobs.Job;
import utils.TelegramUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandro on 5/19/16.
 */
@Every("3s")
public class ReminderJob extends Job{

    public void doJob(){
        reminder();
    }

    public static void reminder(){
        List<Reminder> reminders = Reminder.filter("expiryDate <=", new Date()).asList();
        for (Reminder reminder : reminders){
            TelegramUtils.sendMessage(reminder.message.user.firstName +", me pediste que te recuerde: "
                    + reminder.message.getTextToRemind(), reminder.message.chat.chatId);
            reminder.delete();
        }
    }

}
