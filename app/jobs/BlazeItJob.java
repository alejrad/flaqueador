package jobs;

import models.Chat;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.On;
import utils.TelegramUtils;

import java.util.List;

/**
 * Created by alejandro on 5/25/16.
 */
@On("0 20 19 1/1 * ? *")
public class BlazeItJob extends Job {
    public void doJob(){
        blazeIt();
    }

    public void blazeIt(){
        List<Chat> chats = Chat.findAll();

        for (Chat chat : chats) {
            TelegramUtils.sendMessage("Feliz 4:20 <3", chat.chatId);
        }

    }
}
